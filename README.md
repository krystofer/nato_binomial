Зараз активно обговорюється повоєнна система гарантій безпеки України. А саме вступати в НАТО чи отримати гарантії безпеки окремих країн? (ENG Below)

Є один аспект, який всіма ігнорується в цій дискусії, а саме - кількість союзників чи гарантів безпеки. В цьому дописі я математично продемонструю чому НАТО - набагато кращий варіант для нас, ніж гарантії безпеки від окремих країн. 

Почнемо з того, що наявність союзників або країн-гарантів не гарантує того, що союзник прийде на допомогу в разі потреби. Всі ми пам'ятаємо як спрацювали гарантії країн-підписантів Будапештського меморандуму. Отже, є певна ймовірність того, що країна-союзник чи гарант виконає свої зобов'язання. Позначимо цю ймовірність літерю p. Зрозуміло, що різні країни мають різну надійність виконати свої зобов'язання, але у нашій задачі для спрощення припустимо, що їх надійність однакова. Розглянемо три випадки: низька надійність союзників з низькою ймовірністю прийти на допомогу (p=0.2), середня надійність союзників (p=0.5) і висока надійність союзників (p=0.8 ).

Далі, кількість союзників НАТО є відомою - 30 зараз, а буде 32 після приєднання Фінляндії і Швеції. Кількість потенційних країн-гарантів безпеки України не відома, але навряд чи вона буде великою. Припустимо нехай їх буде 5 (наприклад, США, Великобританія, Польща, Литва, Естонія). 

Припустимо для того щоб країні захиститися успішно від ворожого нападу потрібна допомога якоїсь кількості країн-союзників чи гарантів. Припустимо, що це число має бути хоча б 3, або більше. Але це чило не відіграє критичної ролі у загальних висновках. 

Припустимо також, що союзники приймають рішення про домогу незалежно один від одного. Звісно, у реальному світі це не так, і рішення одних союзників залежить від інших. І одразу скажу, що це мабуть найбільший недолік мого підходу. Але моделювання таких залежностей вимагатиме ще більше припущень і суттєво ускладнить задачу та інтерпритації. 

Отже, нам потрбіно порахувати ймовірність того, що 3 або більше гарантів з 5-ти виконають свої зобов'язання або 3 або більше з 30 країн-членів НАТО. Зі статистики, коли ми маємо порахувати ймовірність успіху k з n спроб, якщо спроби є незалежними, ми маємо справу з біномінальним розподілом. 

На графіку нижче на лівій панелі показані три біномінальні розподіли для різних значень ймовірності p коли в нас кількість гарантів безпеки дорівнює 5. На правій панелі - розподіли для різних p коли в нас кількість союзників НАТО дорівнює 30. Одразу помітно, що більше значення p, тим розподіл зсунутий праворуч, тим більше союзників в середньому відгукнуться на допомогу. Так, середня кількість гарантів, які прийдуть на допомогу (позначимо це значення літерою m) для випадку 5-ти гарантів при p=0.2 становить 1, при p=0.5 становить 2.5, а при p=0.8 становить 4. Поки все логічно, що більш надійні союзники, то більше прийде на допомогу. У випаду НАТО, при p=0.2 в середньому прийде 6 союзників на допомогу, при p=0.5 - 15, при p=0.8 - 24. Звідси вже зрозуміло: що більша кількість союзників в альянсі, то більша кількість прийде на допомогу. 
![](./binomial_distributions_by_n_and_p.png)

Ми можемо підрахувати ймовіність того, що 3 або більше союзників прийдуть на допомогу (позначимо як P(X>=3)). У випадку гарантів безпеки при p=0.2  P(X>=3)=5.79% (вкрай низька!), при p=0.5 P(X>=3)=50% (теж низька), і при p=0.8 P(X>=3)=94.21%. У випадку альянсу НАТО при p=0.2 P(X>=3)=95.58% (одразу дуже висока, хоч союзники в середньому ненадійні), при p=0.5 і p=0.8 P(X>=3) - практично 100%. 

Отже, якщо нам потрібно хоча б 3 союзники щоб відбитися від противника, то у випадку "гарантів безпеки" потрібно щоб всі вони були дуже надійними. У випадку НАТО, навіть якщо всі союзники низьконадійні, то ймовірність знайти хоча б 3 країни, які допоможуть є вищою за 95%. 

Графік нижче ілюструє залежність ймовірністі що хоча б 3 союзники допоможуть проти кількості союзників в альянсі за різних p. Якщо p - висока, скажімо 0.8, то і 5-ти альянтів досить для забезпечення безпеки. Але якщо більш реалістично прийняти, що p=0.5, то для високої ймовірності того, що хоча б 3 союзники потрібно щоб відбитися, то потрібно хоча б 10 союзників в альянсі. Якщо ж союзники ненадійні (p=0.2), то потрбіно 30+ альянтів.  
![](./binomial_relation_of_P_to_n.png)

На величину ймовірності p, тобто на те, чи виконають союзники свої зобов'язання і як саме виконають впливає багато факторів: внутрішньополітична, зовнішньополітична ситуція, економіка тощо. 

Висновок такий: НАТО набагато кращий варіант для нас, ніж "гарантії безпеки", бо країн НАТО просто більше. Чи будуть працювати гарантії безпеки, якщо там буде багато країн? Так, але виглядає так, що політично дуже важко збирати підписантів. В НАТО країни за свій вклад у колективну безпеку отримують гарантії безпеки натомість. А за гарантії безпеки України що отримають країни натомість? Отож. Тому і мало хто хоче таке підписувати. Шлях в НАТО є безальтернативним з цієї точки зору.


The post-war security system is an active topic of debate inside and outside of Ukraine. Specifically, whether Ukraine should join NATO or get “security assurances” from some allied countries. There are many dimensions in this discussion but one aspect is mostly ignored: the impact of the number of allies in NATO or countries vs the number of countries that provide security assurances. Below, I’ll demonstrate mathematically that NATO is a much better option for Ukraine from this perspective.

Having allies or security guarantees does not necessarily mean allies will fulfill their obligations when needed. Remember the infamous Budapest memorandum. More recently Germany and France de facto played on the Russian side during the Minsk agreements compromising Ukrainian sovereignty. Even now after all the atrocities, Russians do on occupied Ukrainian territories France and Germany push Ukraine to surrender territories to Russia to save Putin’s face.
 
So, there is a certain probability an ally will fulfill its obligations. Let’s denote such probability as p. Of course, countries have different fidelity but we will consider p as an average fidelity probability. We will consider three scenarios: low reliability (p=0.2), medium reliability (p=0.5) and high reliability (p=0.8). 
 
NATO has 30 members (32 soon with Finland and Sweden). The number of countries which will provide security guarantees for Ukraine is unknown but it’s unlikely this number will be high. Suppose, this number will be 5 (e.g., USA, UK, Poland, Lithuania, Estonia). Let’s denote this number as n.

A country needs a certain number of allies to defend itself from an enemy. Let’s denote this number as X. Let’s pick k to be 3 or more for illustration purposes. I’ll generalize this number at the end of the thread. Suppose, allies decide whether to help or not independently. This is the strongest assumption in this model. In reality, it’s not the case of course and one can argue that in reality there are positive and negative feedback loops depending on many factors. Modeling statistical systems without the independence of events are generally quite difficult and require even more assumptions. Remember, all models are wrong but some are useful.

So we need to find the probability that three or more allies out of 5 countries that guarantee security or 3 or more out of 30 NATO members will fulfill their obligations. In statistics we Binomial distribution when we need to calculate k out of n successes, which occur with probability p.

Now we can plot binomial distributions for 5 and 30 allies, where each member fulfills its obligations with probability p. Left panel on the plot demonstrates three binomial distributions for three values of p when n=5 (Security guarantees). The right panel demonstrates binomial distributions for different p when n=30 (NATO). The higher value of p the more shifted distribution to the right is. This means that more allies will help on average (let’s denote this number as m). For the case of n=5 (left panel) for p=0.2 m=1, for p=0.5 m=2.5, and for p=0.8 m=4. So far so good, the more reliable allies are (p increases), the more allies on average will respond (m increases). For the case of n=30 (right panel) for p=0.2 m=6, for p=0.5 m=15, and for p=0.8 m=24. If you have more allies in your alliance more will come to help.
![](./binomial_distributions_by_n_and_p.png)

We can also calculate the probability that 3 or more allies will come to help (let’s denote this as P(X>=3)). For the case =5 for p=0.2 P(X>=3)=5.79% (very low), for p=0.5 P(X>=3)=50% (still low), for p=0.8 P(X>=3)=94.21% (high). For the case n=30 for p=0.2 P(X>=3)=95.58% (already high), for p=0.5 and for p=0.8 P(X>=3)=100%. So if we need at least 3 allies to defend ourselves in the case of security guarantees (n=5) we need that all allies to be on average very reliable (p>=0.8). In the case of NATO (n=30) even if allies on average are not very reliable (p=0.2) there will be at least 3 allies to help with the probability over 95%.
This plot demonstrates the dependence of the probability that at least 3 allies will fulfill their obligations (P(X>=3)) vs the number of allies (n) for different p. If p is high, say 0.8, then even 5 allies are enough for security. Whereas, if p is moderate, say 0.5, which is more realistic, for high P(X>=3) you need at least 10 allies. If allies are not reliable, say p=0.2, you need 30 or more allies. 
![](./binomial_relation_of_P_to_n.png)

Conclusion: NATO is a much better option than security guarantees simply because there are much more countries in NATO. Also, NATO has shared logistics, intelligence, common training, unified weapons, and ammo, which are very important for defense. The only option for Ukraine’s long-term security and peace is to join NATO.
