import scipy.stats as stats
import matplotlib.pyplot as plt
import numpy as np


width = 0.3

fig, axs = plt.subplots(1, 2, figsize=(15, 7.5))


for i, n in [(0, 5), (1, 30)]:
    x = np.arange(0, n + 1, 1)

    pmf1 = stats.binom.pmf(x, n, 0.2)
    pmf2 = stats.binom.pmf(x, n, 0.5)
    pmf3 = stats.binom.pmf(x, n, 0.8)

    axs[i].bar(x, stats.binom.pmf(x, n, 0.2), width, label=f'p=0.2, m={n*0.2}, P(X>=3)={round(100 * sum(pmf1[3:]), 2)}%')
    axs[i].bar(x + width, stats.binom.pmf(x, n, 0.5), width, label=f'p=0.5,  m={n*0.5}, P(X>=3)={round(100 * sum(pmf2[3:]), 2)}%')
    axs[i].bar(x + width * 2, stats.binom.pmf(x, n, 0.8), width, label=f'p=0.8,  m={n*0.8}, P(X>=3)={round(100 * sum(pmf3[3:]), 2)}%')
    axs[i].set_ylabel('Probability')
    axs[i].set_xlabel('Number of allies')
    axs[i].legend()
    axs[i].set_title(f'Total Number of allies n={n}')

fig.tight_layout()
plt.savefig('binomial_distributions_by_n_and_p.png', dpi=300)


n_list = range(5, 30)
plt.figure()

for p in [0.2, 0.5, 0.8]:
    probability_3_list = list()
    for n in n_list:
        x = np.arange(0, n + 1, 1)
        pmf = stats.binom.pmf(x, n, p)
        probability_3_list.append(sum(pmf[3:]))
    plt.plot(n_list, probability_3_list, 'o', label=f'p={p}')
plt.ylabel('P(X>=3)')
plt.xlabel('Number of allies, n')
plt.legend()
plt.savefig('binomial_relation_of_P_to_n.png', dpi=300)


